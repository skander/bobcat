import React from "react"
import ApolloClient from "apollo-boost"
import { ApolloProvider } from "@apollo/react-hooks"

import useLocalStorage from "./hooks/localStorage"

import Episode from "./components/Episode"
import EpisodeList from "./components/EpisodeList"

const client = new ApolloClient({
  uri: "http://localhost:4000/graphql"
})

function App() {
  const [currentEpisodeId, setCurrentEpisodeId] = useLocalStorage(
    "bobcat:episode_id",
    null
  )
  return (
    <ApolloProvider client={client}>
      <EpisodeList
        currentEpisodeId={currentEpisodeId}
        onEpisodeSelect={setCurrentEpisodeId}
      />
      <Episode key={currentEpisodeId} episodeId={currentEpisodeId} />
    </ApolloProvider>
  )
}

export default App
