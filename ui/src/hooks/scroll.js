import useThrottle from "./throttle"

export default function useSetScroll(
  el,
  percent,
  delay = 3000,
  dir = "vertical"
) {
  let max = 0
  if (el && dir === "vertical") {
    max = el.scrollHeight //- el.clientHeight
  } else if (el) {
    max = el.scrollWidth //- el.clientWidth
  }

  const scroll = useThrottle(percent * max, delay)

  if (el && dir === "vertical") {
    el.scrollTo({ left: 0, top: scroll, behavior: "smooth" })
  } else if (el) {
    el.scrollTo({ left: scroll, top: 0, behavior: "smooth" })
  }
}
