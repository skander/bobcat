import React, { useState, useEffect, useRef } from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import rangy from "rangy";

import useLocalStorage from "../hooks/localStorage";
import Word from "./Word";
import SegmentMode from "./SegmentMode";
import TagSelect from "./TagSelect";

import { selectionInsideLens, wordSelection } from "../utils/selections";
import { chunkWordsBySegment } from "../utils/segments";

const LIST_SEGMENTS = gql`
  query ListSegments($episodeId: ID!) {
    segments(episodeId: $episodeId) {
      id
      startTime
      endTime
      segType
      tag
    }
  }
`;

const MODIFY_SEGMENTS = gql`
  mutation modifySegments(
    $episodeId: ID!
    $startTime: Float!
    $endTime: Float!
    $segType: String!
    $isAddition: Boolean!
  ) {
    modifySegments(
      episodeId: $episodeId
      startTime: $startTime
      endTime: $endTime
      segType: $segType
      isAddition: $isAddition
    ) {
      id
      startTime
      endTime
      tag
      segType
    }
  }
`;

const MODIFY_SEGMENT = gql`
  mutation modifySegment($segmentId: ID!, $tag: String!) {
    modifySegment(segmentId: $segmentId, tag: $tag) {
      id
      startTime
      endTime
      tag
      segType
    }
  }
`;

const handleMouseUp = ({
  lens,
  segmentMode,
  episodeId,
  segType,
  modifySegments,
}) => {
  const rangySelection = rangy.getSelection();

  if (segmentMode === null) return;

  if (rangySelection.isCollapsed || !selectionInsideLens(lens, rangySelection))
    return;

  const { startTime, endTime } = wordSelection(rangySelection);

  if (startTime === null || endTime === null) return;

  const isAddition = segmentMode === "add";

  // Create segment
  modifySegments({
    variables: { episodeId, startTime, endTime, isAddition, segType },
  });
};

const handleScrollToSegment = (segments, lens, toLast = true) => {
  if (segments && segments.length > 0) {
    let segment;
    if (toLast) {
      // Last segment
      segment = segments
        .slice()
        .sort((s1, s2) => Number(s1.startTime) - Number(s2.startTime))[
        segments.length - 1
      ];
    } else {
      // First without a tag
      segment = segments
        .slice()
        .filter((seg) => seg.tag === null)
        .sort((s1, s2) => Number(s1.startTime) - Number(s2.startTime))[0];
    }
    if (segment) {
      const segDiv = [...lens.children].find(
        (div) => div.getAttribute("data-segment-id") === segment.id
      );
      segDiv.scrollIntoView({ behavior: "smooth" });
    }
  }
};

const useAlignment = (alignmentPath) => {
  const [aligned, setAligned] = useState({});

  useEffect(() => {
    async function fetchData() {
      const result = await fetch(alignmentPath);
      const aligned = await result.json();
      setAligned(aligned);
    }

    fetchData();
  }, [alignmentPath]);

  return aligned;
};

const AlignedTextLens = ({
  controls,
  episodeId,
  alignmentPath,
  currentTime,
  scrollPercent,
  tags,
  ...props
}) => {
  const lens = useRef(null);
  const segType = "character";

  const { words = [] } = useAlignment(alignmentPath);
  // const [segments, setSegments] = useState([]);
  const [segmentMode, setSegmentMode] = useLocalStorage(
    "bobcat:alignment_segment_mode",
    null
  ); // "add", "remove", null

  const { data: { segments } = { segments: [] } } = useQuery(LIST_SEGMENTS, {
    variables: { episodeId },
  });

  // eslint-disable-next-line
  const [modifySegments, _segs] = useMutation(MODIFY_SEGMENTS, {
    update: (cache, { data: { modifySegments: segments } }) => {
      const data = cache.readQuery({
        query: LIST_SEGMENTS,
        variables: { episodeId },
      });
      data.segments = segments;
      cache.writeQuery({
        query: LIST_SEGMENTS,
        variables: { episodeId },
        data,
      });
    },
  });

  // Update single segment, like to update a tag
  // eslint-disable-next-line
  const [modifySegment, _seg] = useMutation(MODIFY_SEGMENT, {
    update: (cache, { data: { modifySegment: segment } }) => {
      const data = cache.readQuery({
        query: LIST_SEGMENTS,
        variables: { episodeId },
      });
      data.segments = data.segments.map((seg) =>
        Number(seg.id) === Number(segment.id) ? segment : seg
      );
      cache.writeQuery({
        query: LIST_SEGMENTS,
        variables: { episodeId },
        data,
      });
    },
  });

  const wordGroups =
    (words && segments && chunkWordsBySegment(words, segments)) || [];

  return (
    <div className={`${props.className} overflow-hidden flex flex-col`}>
      <div className="flex border-bottom p-2 bg-indigo-200 shadow justify-between">
        <h2>AlignedText</h2>
        <div className="flex content-center">
          <span className="text-sm p-1">t: {currentTime}</span>
          <button
            className="p-1 mr-1 rounded cursor-pointer w-30 text-center text-sm bg-gray-300"
            onClick={() => handleScrollToSegment(segments, lens.current, false)}
          >
            First without tag
          </button>
          <button
            className="p-1 rounded cursor-pointer w-30 text-center text-sm bg-gray-300"
            onClick={() => handleScrollToSegment(segments, lens.current)}
          >
            To last
          </button>
          <SegmentMode
            segmentMode={segmentMode}
            setSegmentMode={setSegmentMode}
          />
        </div>
      </div>
      <div
        tabIndex={-1}
        className="p-2 aligned-text overflow-y-scroll h-full"
        onMouseUp={() => {
          handleMouseUp({
            lens: lens.current,
            segmentMode,
            segType,
            episodeId,
            modifySegments,
          });
        }}
        ref={lens}
      >
        {wordGroups &&
          wordGroups.map(({ segment, words }, i) => (
            <React.Fragment key={i}>
              <TagSelect
                segment={segment}
                modifySegment={modifySegment}
                setPlayerTime={controls.setPlayerTime}
                tags={tags && [...tags.characters, "Multiple", "Unknown"]}
              />
              <div>
                {words.map((word, j) => (
                  <Word
                    {...word}
                    key={j}
                    onClick={() => controls.setPlayerTime(word.start)}
                    currentTime={currentTime}
                  />
                ))}
              </div>
            </React.Fragment>
          ))}
      </div>
    </div>
  );
};

export default AlignedTextLens;
