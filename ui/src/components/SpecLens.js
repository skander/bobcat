import React, { useRef } from "react"
import ScrollContainer from "react-indiana-drag-scroll"

import useSetScroll from "../hooks/scroll"

const imagePathIndex = path => parseInt(path.split("_").reverse()[0])

function SpecLens({ imagePaths, scrollPercent, ...props }) {
  const scrollable = useRef(null)
  const el = scrollable.current ? scrollable.current.container.current : null
  useSetScroll(el, Math.max(0, scrollPercent - 0.02), 500, "horizontal")

  return (
    <div className={props.className || ""}>
      <h2>Spec</h2>
      <ScrollContainer
        horizontal={true}
        vertical={false}
        className="flex overflow-x-scroll h-full"
        hideScrollbars={false}
        ref={scrollable}
      >
        {imagePaths
          .slice()
          .sort((a, b) => imagePathIndex(a) - imagePathIndex(b))
          .map((path, i) => (
            <img src={path} key={i} alt={i} />
          ))}
      </ScrollContainer>
    </div>
  )
}

export default SpecLens
