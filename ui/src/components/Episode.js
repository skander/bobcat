import React, { useState, useEffect } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import VideoLens from "./VideoLens";
import RawTextLens from "./RawTextLens";
import AlignedTextLens from "./AlignedTextLens";
import SpecLens from "./SpecLens";

const GET_EPISODE = gql`
  query GetEpisode($id: ID!) {
    episode(id: $id) {
      id
      name
      dataPaths
    }
  }
`;

const noop = () => {};
const defaultControls =
  "play pause playPause getPlayerTime setPlayerTime rewind forward setPlaybackRate"
    .split(" ")
    .reduce((acc, funcName) => ({ ...acc, [funcName]: noop }), {});

const base = "http://localhost:8081";
const mediaUrl = (path) => `${base}/${path}`;
const mediaUrls = (dataPaths) =>
  "aligned_text_path audio_path raw_text_path spec_paths tag_options_path video_path"
    .split(" ")
    .reduce(
      (acc, pathName) => ({
        ...acc,
        [pathName]:
          typeof dataPaths[pathName] === "string"
            ? mediaUrl(dataPaths[pathName])
            : dataPaths[pathName].map((path) => mediaUrl(path)),
      }),
      {}
    );

const useTags = (tagOptionsPath) => {
  const [tags, setTags] = useState(null);
  useEffect(() => {
    async function fetchData() {
      const result = await fetch(tagOptionsPath);
      const tags = await result.json();
      setTags({
        actors: tags.map(t => `${t.actor} (${t.characters.join(", ")})`),
        characters: tags.flatMap(t => t.characters),
      });
    }

    if (tagOptionsPath) {
      fetchData();
    }
  }, [tagOptionsPath]);

  return tags;
};

const handleKeyDown = (event, { setPlayerTime, playPause, rewind, forward }) => {
  const map = {
    75: playPause, // 'k' key (Enter is 32, but conflicts with the video player)
    // 37: () => rewind(0.5), // Left
    72: () => rewind(0.5), // 'h' key
    // 39: () => forward(0.5), // Right
    76: () => forward(0.5), // 'l' key
  }
  const action = map[event.which]
  if (action) {
    event.preventDefault()
    action()
  }
}

// Lenses to show: Video, RawText, AlignedText, Spect
function Episode({ ep, episodeId }) {
  const { loading, error, data } = useQuery(GET_EPISODE, {
    variables: { id: episodeId },
  });

  const { episode } = data || {};

  const {
    aligned_text_path,
    raw_text_path,
    spec_paths,
    tag_options_path,
    video_path,
  } = episode ? mediaUrls(episode.dataPaths) : {};

  const tags = useTags(tag_options_path);
  const [controls, setControls] = useState(defaultControls);
  const [currentTime, setCurrentTime] = useState(0);
  const [duration, setDuration] = useState(0);

  if (!data) {
    if (loading) return "Loading episode...";
    if (error) return `Error loading episode ID ${episodeId}`;
  }

  return (
    <div className="flex flex-col" style={{ height: "100vh" }} tabIndex={-1} onKeyDown={event => handleKeyDown(event, controls)}>
      <div className="flex flex-col" style={{ height: "10vh" }}>
        <h1>Episode {episode.name}</h1>
        <p>Actors: {tags && tags.actors.join(", ")}</p>
        <p>Characters: {tags && tags.characters.join(", ")}</p>
      </div>
      <div className="flex" style={{ height: "70vh" }}>
        <VideoLens
          src={video_path}
          onLoad={setControls}
          onTimeUpdate={setCurrentTime}
          onDurationLoad={setDuration}
          className={"w-1/3"}
        />
        <AlignedTextLens
          episodeId={episodeId}
          alignmentPath={aligned_text_path}
          className={"w-2/3"}
          currentTime={currentTime}
          controls={controls}
          tags={tags}
        />
      </div>
      {/* <div className="flex" style={{ height: "40vh" }}> */}
      {/*   <RawTextLens */}
      {/*     textPath={raw_text_path} */}
      {/*     className={"w-1/3"} */}
      {/*     scrollPercent={currentTime / duration} */}
      {/*   /> */}
      {/* </div> */}
      <div className="flex" style={{ height: "10vh", maxWidth: "100vw" }}>
        <SpecLens
          imagePaths={spec_paths}
          scrollPercent={currentTime / duration}
        />
      </div>
    </div>
  );
}

export default Episode;
