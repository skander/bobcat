import React from "react"

// Flip through adding, removing, and off
const SegmentMode = ({ segmentMode, setSegmentMode }) => {
  const on = segmentMode !== null
  const add = segmentMode === "add"

  return (
    <div className="flex">
      <span className="mr-2 p-1">seg</span>
      <div
        className={`p-1 rounded cursor-pointer w-20 text-center text-sm ${
          add ? "bg-green-200" : on ? "bg-red-200" : "bg-gray-200"
        }`}
        onClick={() => {
          add
            ? setSegmentMode("remove")
            : on
            ? setSegmentMode(null)
            : setSegmentMode("add")
        }}
      >
        {(add && "adding") || (on && "removing") || "off"}
      </div>
    </div>
  )
}

export default SegmentMode
