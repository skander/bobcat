import React from "react"

const Word = ({ word, start, end, currentTime, ...props }) => {
  const hasTime = start && end
  const inTime = hasTime && currentTime >= start && currentTime < end
  const underline = "border-b-2 border-red-500"
  return (
    <span
      data-start-time={start && start.toFixed(2)}
      data-end-time={end && end.toFixed(2)}
      onClick={props.onClick}
      className={
        "word " + (hasTime ? (inTime ? underline : "") : "text-gray-600")
      }
    >
      {word}{" "}
    </span>
  )
}

export default Word
