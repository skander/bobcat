import React, { useState, useEffect, useRef } from "react"
import useSetScroll from "../hooks/scroll"

function RawTextLens({ textPath, scrollPercent, ...props }) {
  const [text, setText] = useState("")

  useEffect(() => {
    async function fetchData() {
      const result = await fetch(textPath)
      const text = await result.text()
      setText(text)
    }

    fetchData()
  }, [textPath])

  const scrollable = useRef(null)
  useSetScroll(scrollable.current, Math.max(0, scrollPercent - 0.025))
  return (
    <div className={`flex flex-col overflow-hidden ${props.className}`}>
      <h2 className="border-bottom p-2 bg-indigo-200 shadow">RawText</h2>
      <div
        className="p-2 overflow-y-scroll flex flex-col h-full"
        ref={scrollable}
      >
        {text.split("\n").map((t, i) => (
          <p key={i}>{t}</p>
        ))}
      </div>
    </div>
  )
}

export default RawTextLens
