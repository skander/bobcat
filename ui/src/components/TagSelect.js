import React from "react";

const TagSelect = ({
  segment,
  modifySegment,
  tags,
  setPlayerTime,
  ...props
}) => {
  // Select input of all potential tags
  // Update segment on select
  const { id, tag, startTime } = segment || {};
  return (
    <div data-segment-id={id} className="mt-2">
      {startTime && (
        <span onClick={() => setPlayerTime(startTime)}>[{startTime}]</span>
      )}
      {startTime && (
        <select
          value={tag || ""}
          onChange={(event) =>
            modifySegment({
              variables: { segmentId: id, tag: event.target.value },
            })
          }
        >
          <option></option>
          {tags &&
            tags.map((val) => (
              <option key={val} value={val}>
                {val}
              </option>
            ))}
        </select>
      )}
      {!startTime && "~"}
    </div>
  );
};

export default TagSelect;
