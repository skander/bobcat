import React from "react"
import { useQuery } from "@apollo/react-hooks"
import gql from "graphql-tag"

const LIST_EPISODES = gql`
  query ListEpisodes {
    episodes {
      id
      name
    }
  }
`

function EpisodeList(
  { currentEpisodeId, onEpisodeSelect } = { onEpisodeSelect: () => {} }
) {
  const { loading, error, data } = useQuery(LIST_EPISODES)

  if (loading) return "Loading episodes..."
  if (error) return "Error loading episodes"

  return (
    <div>
      {data.episodes
        .slice()
        .sort((a, b) => a.name > b.name)
        .map((ep, i) => (
          <span
            key={i}
            className={`mr-1 p-1 cursor-pointer hover:bg-blue-100 ${
              currentEpisodeId === ep.id ? "bg-blue-300" : ""
            }`}
            onClick={() => onEpisodeSelect(ep.id)}
          >
            {ep.name}
          </span>
        ))}
    </div>
  )
}

export default EpisodeList
