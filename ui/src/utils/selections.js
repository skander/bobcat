export const selectionInsideLens = (lens, selection) => {
  return (
    lens.contains(selection.anchorNode) && lens.contains(selection.focusNode)
  )
}

export const wordSelection = selection => {
  let startWord, endWord
  if (selection.isBackward()) {
    startWord = nearestWord(selection, selection.focusNode, {
      isBackward: true,
      isAnchorNode: false
    })
    endWord = nearestWord(selection, selection.anchorNode, {
      isBackward: true
    })
  } else {
    startWord = nearestWord(selection, selection.anchorNode)
    endWord = nearestWord(selection, selection.focusNode, {
      isAnchorNode: false
    })
  }

  return {
    startTime: extractWordMeta(startWord)["data-start-time"],
    endTime: extractWordMeta(endWord)["data-end-time"]
  }
}

const extractWordMeta = word =>
  ["data-start-time", "data-end-time"].reduce(
    (acc, attr) => ({
      ...acc,
      [attr]:
        word.getAttribute(attr) !== null
          ? Number(word.getAttribute(attr))
          : null
    }),
    {}
  )

const nearestWord = (
  selection,
  node,
  { isBackward, isAnchorNode } = { isBackward: false, isAnchorNode: true }
) => {
  let wordElement = nodeElement(node).closest(".word")

  if (wordElement === null) {
    // Backward, get first node from next section
    let wordNodes = selection
      .getRangeAt(0)
      .getNodes()
      .filter(node => hasClassName(nodeElement(node), "word"))
    // Backward, anchorNode => last word of previous section
    // Backward, focusNode => first word of next section
    // Forward, anchorNode => first word of next
    // Forward, focusNode => last word of previous
    if ((isBackward && isAnchorNode) || (!isAnchorNode && !isBackward)) {
      wordElement = nodeElement(wordNodes[wordNodes.length - 1])
    } else {
      console.log(wordNodes[0])
      wordElement = nodeElement(wordNodes[0])
    }
  }
  return wordElement
}

export const rangeOverlaps = ([start1, end1], [start2, end2]) =>
  end1 > start2 && end2 > start1
export const rangeOverlapsInclusive = ([start1, end1], [start2, end2]) =>
  end1 >= start2 && end2 >= start1
export const rangeContains = ([start, end], point) =>
  start <= point && point < end

const nodeElement = node => (node.nodeType === 3 ? node.parentElement : node)

const hasClassName = (element, className) =>
  (" " + element.className + " ").indexOf(" " + className + " ") >= 0
