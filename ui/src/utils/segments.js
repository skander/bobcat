import memoize from "memoize-one";

import { rangeOverlaps } from "./selections";

const overlappedSegments = (segments, segment) => {
  // Cases: same segment, different segment, contained segments
  return segments.filter((seg) => {
    // from: https://eli.thegreenplace.net/2008/08/15/intersection-of-1d-segments
    // with s1 = selection and s2 = sel:
    // s1_end >= s2_start && s2_end >= s1_start
    return rangeOverlaps(
      [segment.startTime, segment.endTime],
      [seg.startTime, seg.endTime]
    );
  });
};

const equivalentAttributes = [
  "startTime",
  "endTime",
  "isPending",
  "segType",
  "tag",
];
const equivalenceAttrs = (segment) =>
  Object.assign(
    {},
    ...equivalentAttributes.map((attr) => ({ [attr]: segment[attr] }))
  );

export const addSegment = (segments, { startTime, endTime }) => {
  const segment = { startTime, endTime };
  if (startTime && endTime) {
    const segmentExists = segments.find(
      (seg) =>
        JSON.stringify(equivalenceAttrs(seg)) ===
        JSON.stringify(equivalenceAttrs(segment))
    );

    // Not already in selections list
    if (!segmentExists) {
      const overlapped = overlappedSegments(segments, segment);

      const minStart = Math.min(
        ...[...overlapped, segment].map((seg) => seg.startTime)
      );
      const maxEnd = Math.max(
        ...[...overlapped, segment].map((seg) => seg.endTime)
      );
      return [
        ...segments.filter((seg) => overlapped.indexOf(seg) < 0),
        { startTime: minStart, endTime: maxEnd, isPending: true },
      ];
    } else {
      return segments;
    }
  }
};

export const removeSegment = (segments, { startTime, endTime }) => {
  const overlaps = overlappedSegments(segments, { startTime, endTime });
  if (overlaps.length === 0) return segments;
  const minStart = Math.min(...overlaps.map((seg) => seg.startTime));
  const maxEnd = Math.max(...overlaps.map((seg) => seg.endTime));

  let startSegment, endSegment;
  if (minStart < startTime) {
    startSegment = {
      startTime: minStart,
      endTime: startTime,
      isPending: true,
    };
  }
  if (endTime < maxEnd) {
    endSegment = {
      startTime: endTime,
      endTime: maxEnd,
      isPending: true,
    };
  }

  return [
    ...segments.filter((seg) => overlaps.indexOf(seg) < 0),
    startSegment,
    endSegment,
  ].filter((seg) => seg);
};

export const areDifferent = (segments_1 = [], segments_2 = []) => {
  if (typeof segments_1 === "undefined" || typeof segments_2 === "undefined")
    return true;

  const diffLength = segments_1.length !== segments_2.length;
  if (diffLength) return true;

  const segs_1 = segments_1
    .map(equivalenceAttrs)
    .sort((a, b) => Number(a.startTime) - Number(b.startTime));
  const segs_2 = segments_2
    .map(equivalenceAttrs)
    .sort((a, b) => Number(a.startTime) - Number(b.startTime));
  return JSON.stringify(segs_1) !== JSON.stringify(segs_2);
};

export const chunkSegmentsByTag = (segments) => {
  return segments.reduce((acc, seg) => {
    let tag = seg.tag;
    let [lastGroup, ...reverseRest] = acc.slice().reverse();

    if (typeof lastGroup === "undefined")
      return [
        {
          tag: tag,
          startTime: seg.startTime,
          endTime: seg.endTime,
          segments: [seg],
        },
      ];

    let { tag: prevTag } = lastGroup;
    let rest = reverseRest.reverse();

    if (prevTag !== tag) {
      return acc.concat({
        tag: tag,
        startTime: seg.startTime,
        endTime: seg.endTime,
        segments: [seg],
      });
    } else {
      return [
        ...rest,
        {
          ...lastGroup,
          endTime: seg.endTime,
          segments: [...lastGroup.segments, seg],
        },
      ];
    }
  }, []);
};

export const chunkWordsBySegment = memoize(
  (words, segments) => {
    if (typeof words[0] === "undefined" || typeof segments === "undefined") {
      return [];
    }

    // Create chunks of words/segments of the form:
    // {segment: {startTime, endTime, tag, segType}, words: [], startTime, endTime}
    segments = segments
      .slice()
      .sort((a, b) => Number(a.startTime) - Number(b.startTime));

    let currentSegment = segments.shift() || null;

    if (currentSegment === null) {
      return [
        {
          segment: null,
          startTime: words[0].startTime,
          endTime: words[words.length - 1].endTime,
          words: words,
        },
      ];
    }

    // debugger;
    return words.reduce((acc, word, index) => {
      let [lastGroup, ...reverseRest] = acc.slice().reverse();

      let wordInSeg = wordInSegment(word, currentSegment);

      if (index === 0) {
        return [
          {
            segment: wordInSeg ? currentSegment : null,
            startTime: word.start,
            endTime: word.end,
            words: [word],
          },
        ];
      }

      if (
        !wordInSeg &&
        currentSegment &&
        round(word.start) >= round(currentSegment.endTime)
      ) {
        currentSegment = segments.shift();

        wordInSeg = wordInSegment(word, currentSegment);
        return [
          ...reverseRest.reverse(),
          lastGroup,
          {
            segment: wordInSeg ? currentSegment : null,
            startTime: word.start,
            endTime: word.end,
            words: [word],
          },
        ];
      }

      if (wordInSeg && sameTime(word.end, currentSegment.endTime)) {
        acc = [
          ...reverseRest.reverse(),
          {
            ...lastGroup,
            segment: currentSegment,
            endTime: word.end,
            words: [...lastGroup.words, word],
          },
        ];

        currentSegment = segments.shift();

        if (index !== words.length - 1) {
          acc.push({
            segment: null,
            startTime: null,
            endTime: null,
            words: [],
          });
        }
        return acc;
      }

      // Cases:
      // 1. Word at start of segment ->
      // if previous group is blank, replace last group with this word.
      // else create new group with segment
      if (
        wordInSeg &&
        (sameTime(word.start, currentSegment.startTime) ||
          lastGroup.segment === null)
      ) {
        if (lastGroup.words.length === 0) {
          return [
            ...reverseRest.reverse(),
            {
              ...lastGroup,
              segment: currentSegment,
              startTime: Number(currentSegment.startTime),
              endTime: Number(currentSegment.endTime),
              words: [...lastGroup.words, word],
            },
          ];
        }

        acc.push({
          segment: currentSegment,
          startTime: Number(currentSegment.startTime),
          endTime: Number(currentSegment.endTime),
          words: [word],
        });
        return acc;
      }

      // 2. Word at end of segment, add to previous group and get next segment, create new empty group if not at end of file
      // Word in segment or no timestamps, add to previous group
      return [
        ...reverseRest.reverse(),
        { ...lastGroup, words: [...lastGroup.words, word] },
      ];
    }, []);
  },
  ([words, segments], [oldWords, oldSegments]) => {
    // if segments are different, return false
    if (areDifferent(segments, oldSegments)) {
      return false;
    }

    // if words are different, return false
    if (words !== oldWords) {
      return false;
    }
    return true;
  }
);

const wordInSegment = (word, segment) =>
  word &&
  segment &&
  word.start &&
  segment.startTime &&
  rangeOverlaps(
    [round(word.start), round(word.end)],
    [round(segment.startTime), round(segment.endTime)]
  );

const sameTime = (time_1, time_2) =>
  time_1 && time_2 && round(time_1) === round(time_2);

const round = (number) => Math.round(number * 100) / 100;
