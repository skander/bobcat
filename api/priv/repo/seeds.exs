# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs

alias Bobcat.Core.Episode
alias Bobcat.{Core, Repo}

import Ecto.Query

# Episode
# video_path, raw_text_path, aligned_text_path, audio_path, tag_options_path, ...
data_root = Path.expand("../data")
videos_path = Path.join(data_root, "video/mp4")

videos_path
|> File.ls!()
|> Enum.each(fn video_file ->
  ep = Path.rootname(video_file)

  video_path = "video/mp4/#{video_file}"
  raw_text_path = "text/raw/#{ep}.txt"
  aligned_text_path = "text/aligned/#{ep}.json"
  audio_path = "audio/mp3/#{ep}.mp3"

  spec_paths =
    Path.join(data_root, "spectrograms/#{ep}")
    |> File.ls!()
    |> Enum.map(fn num -> "spectrograms/#{ep}/#{num}" end)

  tag_options_path = "text/tag_options/#{ep}.json"

  data_paths = %{
    video_path: video_path,
    raw_text_path: raw_text_path,
    aligned_text_path: aligned_text_path,
    audio_path: audio_path,
    spec_paths: spec_paths,
    tag_options_path: tag_options_path
  }

  Episode
  |> where(name: ^ep)
  |> limit(1)
  |> Repo.one()
  |> case do
    nil ->
      attrs = %{name: ep, data_paths: data_paths}
      Core.create_episode(attrs)

    _ ->
      IO.puts("#{ep} exists")
  end
end)

Path.join(data_root, "labels/e01_characters.json")
|> File.read!()
|> Jason.decode!(e01_segments_json)
|> Enum.map(&Bobcat.Core.create_segment/1)
