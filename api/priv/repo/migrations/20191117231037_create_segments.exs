defmodule Bobcat.Repo.Migrations.CreateSegments do
  use Ecto.Migration

  def change do
    create table(:segments) do
      add :start_time, :float
      add :end_time, :float
      add :duration, :float
      add :seg_type, :string
      add :episode_id, references(:episodes, on_delete: :nothing)

      timestamps()
    end

    create index(:segments, [:episode_id])
  end
end
