defmodule Bobcat.Repo.Migrations.AddTagToSegments do
  use Ecto.Migration

  def change do
    alter table(:segments) do
      add :tag, :string
    end
  end
end
