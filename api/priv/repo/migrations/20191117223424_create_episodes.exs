defmodule Bobcat.Repo.Migrations.CreateEpisodes do
  use Ecto.Migration

  def change do
    create table(:episodes) do
      add :name, :string
      add :data_paths, :map

      timestamps()
    end

  end
end
