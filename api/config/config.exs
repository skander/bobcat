# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :bobcat,
  ecto_repos: [Bobcat.Repo]

# Configures the endpoint
config :bobcat, BobcatWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "0zafAUpbtmf1x1eIdUJReyUlT1oGr5v6yhLjBVebAuhSlixzsvNhsVo/sCy3Lzu1",
  render_errors: [view: BobcatWeb.ErrorView, accepts: ~w(json)],
  pubsub_server: Bobcat.PubSub

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
