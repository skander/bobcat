defmodule Bobcat.Repo do
  use Ecto.Repo,
    otp_app: :bobcat,
    adapter: Ecto.Adapters.Postgres
end
