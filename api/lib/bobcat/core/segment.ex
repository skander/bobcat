defmodule Bobcat.Core.Segment do
  use Ecto.Schema
  import Ecto.Changeset

  schema "segments" do
    field :start_time, :float
    field :end_time, :float
    field :duration, :float
    field :seg_type, :string
    field :tag, :string # Actor name or character name - label, basically

    timestamps()

    belongs_to :episode, Bobcat.Core.Episode
  end

  @doc false
  def changeset(segment, attrs) do
    segment
    |> cast(attrs, [:start_time, :end_time, :duration, :seg_type, :tag, :episode_id])
    |> validate_required([:start_time, :end_time, :duration, :seg_type, :episode_id])
  end
end
