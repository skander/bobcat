defmodule Bobcat.Core.Episode do
  use Ecto.Schema
  import Ecto.Changeset

  alias Bobcat.Core.Segment

  schema "episodes" do
    field :data_paths, :map
    field :name, :string

    timestamps()

    has_many :segments, Segment
  end

  @doc false
  def changeset(episode, attrs) do
    episode
    |> cast(attrs, [:name, :data_paths])
    |> validate_required([:name, :data_paths])
  end
end
