defmodule Bobcat.Core.SegmentCalculator do
  def add_segment(segments, %{start_time: start_time, end_time: end_time} = segment) do
    case Enum.any?(segments, &(equivalence_attrs(&1) == equivalence_attrs(segment))) do
      true ->
        segments

      false ->
        overlaps = overlapped_segments(segments, segment)
        min_start = [start_time | overlaps |> Enum.map(&Map.get(&1, :start_time))] |> Enum.min()
        max_end = [end_time | overlaps |> Enum.map(&Map.get(&1,:end_time))] |> Enum.max()

        [%{segment | start_time: min_start, end_time: max_end} | segments -- overlaps]
    end
  end

  def remove_segment(segments, %{start_time: start_time, end_time: end_time} = segment) do
    overlaps = overlapped_segments(segments, segment)

    case Enum.any?(overlaps) do
      false ->
        segments

      true ->
        min_start = overlaps |> Enum.map(& Map.get(&1, :start_time) |> number_val()) |> Enum.min()
        max_end = overlaps |> Enum.map(& Map.get(&1, :end_time) |> number_val()) |> Enum.max()

        results = []

        results =
          if min_start < start_time do
            [%{segment | start_time: min_start, end_time: start_time} | results]
          else
            results
          end

        results =
          if end_time < max_end do
            [%{segment | start_time: end_time, end_time: max_end} | results]
          else
            results
          end

        (segments -- overlaps) ++ results
    end
  end

  def range_overlaps?(%{start_time: start_1, end_time: end_1}, %{
         start_time: start_2,
         end_time: end_2
       }) do
   number_val(end_1) > number_val(start_2) && number_val(end_2) > number_val(start_1)
  end

  def range_contains?(%{start_time: start_time, end_time: end_time}, time) do
    number_val(start_time) <= number_val(time) && number_val(time) < number_val(end_time)
  end

  defp equivalence_attrs(%{start_time: start_time, end_time: end_time}),
    do: %{start_time: number_val(start_time), end_time: number_val(end_time)}

  defp overlapped_segments(segments, segment) do
    segments
    |> Enum.filter(&range_overlaps?(&1, segment))
  end

  defp number_val(%Decimal{} = number), do: Decimal.to_float(number)
  defp number_val(number) when is_float(number) or is_integer(number), do: number
end
