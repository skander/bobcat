defmodule Bobcat.Core do
  import Ecto.Query, warn: false

  alias Bobcat.Repo
  alias Bobcat.Core.{Episode, Segment, SegmentCalculator}

  # CRUD function generator
  # [:get, :list, :count, :search, :filter, :create, :update, :delete]
  require Crudry.Context
  Crudry.Context.generate_functions(Episode)
  Crudry.Context.generate_functions(Segment)

  def modify_segments(
        segments,
        %{
          episode_id: episode_id,
          is_addition: is_addition
        } = segment,
        episode_id
      ) do
    # Calculate segments, async insert to db
    new_segments =
      case is_addition do
        true -> SegmentCalculator.add_segment(segments, segment)
        false -> SegmentCalculator.remove_segment(segments, segment)
      end

    store_segment_diff(segments, new_segments, episode_id)
    |> case do
      {:ok, _results} -> filter_segments(%{episode_id: episode_id})
      error -> error
    end
  end

  def store_segment_diff(segments, new_segments, episode_id) do
    # Get old segments that don't show up in the new ones
    removals =
      segments
      |> Enum.filter(fn seg ->
        new_segments
        |> Enum.any?(&(equivalence_attrs(&1) == equivalence_attrs(seg)))
        |> Kernel.not()
      end)

    # Get new segments that don't show up in the old ones
    additions =
      new_segments
      |> Enum.filter(fn seg ->
        segments
        |> Enum.any?(&(equivalence_attrs(&1) == equivalence_attrs(seg)))
        |> Kernel.not()
      end)
      |> Enum.map(
        &Map.merge(&1, %{
          episode_id: episode_id,
          duration: Float.round(&1.end_time - &1.start_time, 2)
        })
      )
      |> Enum.map(&Segment.changeset(%Segment{}, &1))

    multi = Ecto.Multi.new()

    multi =
      if Enum.any?(removals) do
        removals
        |> Enum.reduce(
          multi,
          fn %{start_time: start_time, end_time: end_time} = removal, multi ->
            Ecto.Multi.delete(
              multi,
              {:delete_segment, %{start_time: start_time, end_time: end_time, id: removal.id}},
              removal
            )
          end
        )
      else
        multi
      end

    multi =
      if Enum.any?(additions) do
        additions
        |> Enum.reduce(
          multi,
          fn %{changes: %{start_time: start_time, end_time: end_time}} = addition, multi ->
            Ecto.Multi.insert(
              multi,
              {:insert_segment, %{start_time: start_time, end_time: end_time}},
              addition
            )
          end
        )
      else
        multi
      end

    Repo.transaction(multi)
  end

  def equivalence_attrs(%{start_time: start_time, end_time: end_time}),
    do: %{start_time: decimalize(start_time), end_time: decimalize(end_time)}

  def decimalize(%Decimal{} = number), do: number
  def decimalize(number) when is_integer(number), do: Decimal.new(number)
  def decimalize(number) when is_float(number), do: Decimal.from_float(number)
end
