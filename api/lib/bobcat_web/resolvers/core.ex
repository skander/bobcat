defmodule BobcatWeb.Resolver.Core do
  alias Bobcat.Core
  alias Bobcat.Core.{Episode, Segment}
  require Crudry.Resolver

  Crudry.Resolver.generate_functions Core, Episode, except: [:get]
  Crudry.Resolver.generate_functions Core, Segment, except: [:list]

  # Episode
  # list, show
  def get_episode(%{id: id}, _info) do
    id
    |> Core.get_episode_with_assocs([:segments])
    |> nil_to_error("episode", fn record -> {:ok, record} end)
  end

  # Segment
  # list for episode, create, destroy
  def list_segments(%{episode_id: episode_id}, _info) do
    {:ok, Core.filter_segments(%{episode_id: episode_id})}
  end

  def modify_segment(%{segment_id: id, tag: tag}, _info) do
    with segment <- Core.get_segment(id),
         {:ok, segment} <- Core.update_segment(segment, %{tag: tag}) do
      {:ok, segment}
    else
      {:error, _error} = err -> err
      error -> {:error, error}
    end
  end

  def modify_segments(%{episode_id: episode_id} = segment, _info) do
    with segments when is_list(segments) <- Core.filter_segments(%{episode_id: episode_id}) do
      {:ok, Core.modify_segments(segments, segment, episode_id)}
    else
      {:error, _error} = err -> err
      error -> {:error, error}
    end
  end
end
