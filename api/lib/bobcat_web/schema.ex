defmodule BobcatWeb.Schema do
  use Absinthe.Schema

  alias BobcatWeb.Resolver.Core

  import_types BobcatWeb.Type.Core

  query do
    field :episodes, list_of(:episode) do
      resolve &Core.list_episodes/2
    end

    field :episode, :episode do
      arg :id, :id

      resolve &Core.get_episode/2
    end

    field :segments, list_of(:segment) do
      arg :episode_id, :id

      resolve &Core.list_segments/2
    end
  end

  mutation do
    field :modify_segment, :segment do
      arg :segment_id, :id
      arg :tag, :string

      resolve &Core.modify_segment/2
    end

    field :modify_segments, list_of(:segment) do
      arg :episode_id, :id
      arg :start_time, :float
      arg :end_time, :float
      arg :seg_type, :string
      arg :is_addition, :boolean

      resolve &Core.modify_segments/2
    end
  end
end
