defmodule BobcatWeb.Router do
  use BobcatWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", BobcatWeb do
    pipe_through :api
  end

  if Mix.env() == :dev do
  end

  scope "/" do
    pipe_through :api

    forward "/graphiql",
            Absinthe.Plug.GraphiQL,
            schema: BobcatWeb.Schema,
            json_codec: Jason,
            interface: :simple

    forward "/graphql", Absinthe.Plug,
      schema: BobcatWeb.Schema,
      json_codec: Jason
  end
end
