defmodule BobcatWeb.Type.Core do
  use Absinthe.Schema.Notation

  import_types(Absinthe.Type.Custom)
  import_types(BobcatWeb.Type.Json)

  @desc "Episode"
  object :episode do
    field :id, :id
    field :name, :string
    field :data_paths, :json

    field :segments, list_of(:segment)
  end

  @desc "Segment defines important time ranges"
  object :segment do
    field :id, :id
    field :episode, :episode
    field :start_time, :float
    field :end_time, :float
    field :duration, :float
    field :seg_type, :string
    field :tag, :string

    field :is_pending, :boolean,
      resolve: fn _, _, _ ->
        {:ok, false}
      end
  end
end
